﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void write(string text)
        {
            Console.WriteLine(text);
        }

        static void pause()
        {
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            string hello = "Hello", world;
            world = "World";
            write(hello+" "+world);

            string helloru, worldru;
            helloru = "Привет";
            worldru = "Мир";
            write(helloru+" "+worldru);

            string hernya;
            write("Напишите какую-нибудь херню:");
            hernya = Console.ReadLine();
            write("херня: " + hernya);
            pause();
        }
    }
}
